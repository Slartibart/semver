# SemVer

SemVer is a CLI tool to find the highest of two version numbers which follow the
[Semantic Version Specification](https://semver.org/).

## Usage

Call semver with the `-c` flag follwed by two version strings.

| Output | Description |
| :---: | :--- |
| 1 | First version is greater |
| 0 | Versions are identical |
| -1 | Second version is greater |

```
$ semver -c 1.4.2 1.3.2
1
```
