package main

import (
    "github.com/google/go-cmp/cmp"
    "testing"
)


func TestCompareVersionsFirst(t *testing.T) {
    first := "1.5.2"
    second := "1.2.1"
    
    got := compareVersions(first, second)

    if got != 1 {
        t.Fatalf(`compareVersions(1.5.2, 1.2.1) = %q, want %v`, got, first)
    }
}


func TestCompareVersionsSecond(t *testing.T) {
    first := "1.5.2"
    second := "2.2.1"
    
    got := compareVersions(first, second)

    if got != -1 {
        t.Fatalf(`compareVersions(1.5.2, 2.2.1) = %q, want %v`, got, second)
    }
}


func TestCompareVersionsEqual(t *testing.T) {
    first := "1.5.2"
    second := "1.5.2"
    
    got := compareVersions(first, second)

    if got != 0 {
        t.Fatalf(`compareVersions(1.5.2, 1.5.2) = %q, want %v`, got, first)
    }
}

func TestStringToVersionString(t *testing.T) {
    string := "1.5.2"
    want := Version{1, 5, 2}

    got := stringToVersion(string)

    result := cmp.Equal(got, want)

    if !result {
        t.Fatalf(`stringToVersion(1.5.2) = %q, want %v`, got, want)
    }
}

func TestVersionToStringVersion(t *testing.T) {
    version := Version{1, 5, 2}
    want := "1.5.2"

    got := versionToString(version)

    result := cmp.Equal(got, want)

    if !result {
        t.Fatalf(`versionToString(Version{}) = %q, want %v`, got, want)
    }
}
