package main

import (
    "flag"
    "fmt"
    "strconv"
    "strings"
)

type Version struct {
    Major, Minor, Patch int
}


func main() {
    var cFlag string
    flag.StringVar(&cFlag, "c", "", "Two version numbers to compare")

    flag.Parse()

    args := []string(flag.Args())

    if cFlag != "" {
        //fmt.Println(args[:])
        fmt.Println(compareVersions(cFlag, args[0]))
    }
}


func compareVersions(one string, two string) int {
    versionOne := stringToVersion(one)
    versionTwo := stringToVersion(two)
    
    // Major
    if versionOne.Major > versionTwo.Major {
        return 1
    } else if versionOne.Major < versionTwo.Major {
        return -1
    } else {
        // Minor
        if versionOne.Minor > versionTwo.Minor {
            return 1
        } else if versionOne.Minor < versionTwo.Minor {
            return -1
        } else {
            // Patch
            if versionOne.Patch > versionTwo.Patch {
                return 1
            } else if versionOne.Patch < versionTwo.Patch {
                return -1
            } else {
                // Equal
                return 0
            }
        }
    }
}


func versionToString(version Version) string {
    Major := strconv.Itoa(version.Major)
    Minor := strconv.Itoa(version.Minor)
    Patch := strconv.Itoa(version.Patch)
    
    versionSlice := []string{Major, Minor, Patch}

    versionString := strings.Join(versionSlice, ".")

    return versionString
}


func stringToVersion(version string) Version {
    strings := strings.Split(version, ".")

    Major, _ := strconv.Atoi(strings[0])
    Minor, _ := strconv.Atoi(strings[1])
    Patch, _ := strconv.Atoi(strings[2])

    return Version{Major, Minor, Patch}
}
